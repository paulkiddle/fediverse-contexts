import fs from 'fs';
import contexts from './src/context/build.js';

let js = '';
let ts = '';

for(const [name, terms] of Object.entries(contexts)) {
	const value = JSON.stringify(terms);
	js += `export const ${name} = ${value};\n`;
	ts += `export const ${name} = ${value} as const;\n`;
}

fs.writeFileSync('./dist/main.js', js);
fs.writeFileSync('./dist/main.ts', ts);
