import activitystreams from './activitystreams.js';
import { contextMap } from '../context-map.js';
import security from './security-v1.js';
import toot from './toot.js';

export default await contextMap({
	as: activitystreams['@context'],
	sec: security['@context'],
	toot
});
