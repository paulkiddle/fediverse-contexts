import jsonld from 'jsonld';

const toObj = value => ({
	'@id': value,
	'@type': '@id' 
});
const termToObj = value => typeof value === 'string' ? toObj(value) : value;
const elongate = async (context, value) => (await jsonld.expand({
	'@context': context,
	...termToObj(value) 
}))[0]['@id'];

// Create a map of contexts short names to full URLs, for utility
// there's probaly a faster way to do this but it works and only runs on build
export async function contextMap(contextMap) {
	const contexts = Object.values(contextMap);
	return Object.fromEntries(await Promise.all(Object.entries(contextMap).map(async ([key,context])=>{
		const entries = await Promise.all(Object.entries(context).map(async ([k, v]) => [k, await elongate(contexts, v)]));
		return [key, Object.fromEntries(entries)];
	})));
}
